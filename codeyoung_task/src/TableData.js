import { useEffect, useState } from 'react';
import axios from 'axios';
import { Table, Container, Spinner } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.css';

const TableData = () => {

    const [gitData, setGitData] = useState([]);
    const [pageNumber, setPageNumber] = useState(1);
    const [hitApi, setHitApi] = useState(true);
    const [loader, setLoader] = useState(false);

    const getGitDetails = () => {
        setHitApi(false);
        setPageNumber(pageNumber + 1)
        setLoader(true);
        axios.get(`https://api.github.com/repos/neovim/neovim/pulls`, { params: { page: pageNumber } })
            .then(res => {
                setGitData([...gitData, ...res.data]);
                setHitApi(true);
                setLoader(false);
            })
            .catch(err => {
                console.log('err', err)
                setHitApi(true);
                setLoader(false);
            })
    }

    useEffect(() => {
        getGitDetails();

    }, [])

    window.onscroll = function (ev) {
        let scrollHeight = Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
        );
        let currentScrollHeight = window.innerHeight + window.scrollY;

        if ((scrollHeight - currentScrollHeight) < 40) {
            if (hitApi) {
                getGitDetails();
            }
        }
    };

    return (
        <>
            <Container>
                <Table hover>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Title</th>
                            <th>Base Branch</th>
                            <th>Author Branch</th>
                            <th>Author</th>
                            <th>Created On</th>
                            <th>Reviewers</th>
                            <th>Labels</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            gitData.map((el, index) => {
                                return (
                                    <tr key={index}>
                                        <th scope="row">{index + 1}</th>
                                        <td>{el.user.login}</td>
                                        <td>{el.base.label}</td>
                                        <td>{el.head.label}</td>
                                        <td>{el.author_association}</td>
                                        <td>{el.created_at}</td>
                                        <td>
                                            {
                                                <ol>
                                                    {
                                                        el?.requested_reviewers?.map((eli, eliIndex) => {
                                                            return (
                                                                <li key={eliIndex}>{eli.login}</li>
                                                            )

                                                        })
                                                    }
                                                </ol>
                                            }
                                        </td>
                                        <td>
                                            {
                                                <ol>
                                                    {
                                                        el?.labels?.map((eli, eliIndex) => {
                                                            return (
                                                                <li key={eliIndex}>{eli.name}</li>
                                                            )

                                                        })
                                                    }
                                                </ol>
                                            }
                                        </td>
                                    </tr>
                                )
                            })
                        }

                    </tbody>
                </Table>

                {
                    loader
                        ?
                        <div>
                            <center>
                                <Spinner type="grow" color="primary" />
                                <Spinner type="grow" color="secondary" />
                                <Spinner type="grow" color="success" />
                                <Spinner type="grow" color="danger" />
                                <Spinner type="grow" color="warning" />
                                <Spinner type="grow" color="info" />
                                <Spinner type="grow" color="light" />
                                <Spinner type="grow" color="dark" />
                            </center>
                        </div>
                        :
                        ''
                }

            </Container>
        </>
    )
}

export default TableData;